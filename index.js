const debug = require('debug')('RWorker');
const errors = require('./const/errors.js');

class RWorker {
    constructor(params) {
        let self = this;
        this.r = require('rethinkdb');
        if (params instanceof Object)
            for (let key in params) {
                this[key] = params[key];
            }
        else {
            throw new Error(errors.CONSTRUCTOR_NO_PARAMS);
        }

        this.subscriptions = {};
        this.cursors = {};
        this.counters = {};
        this.dispetchers = {};

        this.init = (async function () {
            try {
                self.conn = await self.r.connect({
                    db: self.db
                });
                return self.conn;

            } catch (error) {
                debug(error);
                throw new Error(errors.R_CONN_ERROR);
            }
        })();
    }

    createCounter(name) {
        if (this.counters.hasOwnProperty(name) || !name)
            return null;
        this.counters[name] = 0;
    }

    increaseCounter(name, increment) {
        if (!increment)
            increment = 1;
        if (!this.counters.hasOwnProperty(name))
            return null;
        return this.counters[name] += increment;
    }

    reduceCounter(name, decrement) {
        if (!decrement)
            decrement = 1;
        if (!this.counters.hasOwnProperty(name))
            return null;
        return this.counters[name] -= decrement;
    }

    readCounter(name) {
        return this.counters[name];
    }

    createDispatcher(name, params, action, timeInterval) {
        params.self = this;
        if (this.dispetchers.hasOwnProperty(name) || !params.count || !action)
            return null;
        if (!timeInterval)
            timeInterval = 3000;
        this.dispetchers[name] = setInterval(action, timeInterval, params);
        return name;
    }

    async createSubscription(name, filter, reactions, params) {
        try {
            debug('start createSubscription');
            if (name)
                this.subscriptions[name] = true;
            if (!params) {
                params = [null];
            }
            else if (!(params instanceof Array))
                params = [params];

            let self = this;
            let conn = await this.init;

            if (filter.name === 'getAll') {
                if (filter.indexName && filter.indexValue)
                    this.r.table(this.table).getAll(filter.indexValue, {index: filter.indexName}).changes().run(conn, (err, cursor) => {
                        if (err)
                            throw err;

                        else if (!self.subscriptions[name])
                            cursor.close();

                        else {
                            self.cursors[name] = cursor;
                            cursor.each((err, data) => {
                                if (err) throw err;
                                if (data.new_val !== null) {
                                    let changedData = data.new_val;
                                    reactions.apply(this, [
                                        changedData,
                                        ...params
                                    ]);
                                }
                            });
                        }

                    });
            }
            else if (filter.name === 'get') {
                if (filter.Value)
                    this.r.table(this.table).get(filter.id).changes().run(conn, (err, cursor) => {
                        if (err)
                            throw err;

                        else if (!self.subscriptions[name])
                            cursor.close();
                        else {
                            self.cursors[name] = cursor;
                            cursor.each((err, data) => {
                                if (err) throw err;
                                if (data.new_val !== null) {
                                    let changedData = data.new_val;
                                    reactions.apply(this, [
                                        changedData,
                                        ...params
                                    ]);
                                }
                            });
                        }
                    });
            }
            else if (filter.name === 'filter') {
                if (filter.params instanceof Object)
                    this.r.table(this.table).filter(filter.params).changes().run(conn, (err, cursor) => {
                        if (err)
                            throw err;

                        else if (!self.subscriptions[name])
                            cursor.close();

                        else {
                            self.cursors[name] = cursor;
                            cursor.each((err, data) => {
                                if (err) throw err;
                                if (data.new_val !== null) {
                                    let changedData = data.new_val;
                                    reactions.apply(this, [
                                        changedData,
                                        ...params
                                    ]);
                                }
                            });
                        }

                    });
            }
            return 1;
        } catch (error) {
            debug(error);
            throw error;
        }
    }

    deleteSubscription(name) {
        if (this.subscriptions.hasOwnProperty(name)) {
            this.subscriptions[name] = false;
            return name;
        } else {
            return null
        }
    }

    async destroy() {
        try {
            for (let key in this.subscriptions) {
                this.subscriptions[key] = false;
                debug('unsub: ', key);
            }
            for (let key in this.dispetchers) {
                clearInterval(this.dispetchers[key]);
                debug('clearInterverl: ', key);
            }
            for (let key in this.cursors) {
                await this.cursors[key].close();
                debug('close cursor: ', key);
            }

            let conn = await this.init;
            await conn.close();

            return true;
        }

        catch (error) {
            debug(error);
            throw error;
        }
    }
}


module.exports = exports = RWorker;